/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiposdedato;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author a_lej
 */ 
public class TiposDeDato {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // Entrada de datos

        /* BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Escribe un numero: ");
        String num1 = buffer.readLine();
        
        System.out.println("Escribe un numero: ");
        int num2 = buffer.read();
        
        System.out.println(num1);
        System.out.println(num2); */
        
        // tipos de conversion
       /* double a = 9.888888;
        int b = 5;
        String c = "4";
        
        
        // implicita
        double d = b;
        // explicita
        int e = (int) a;
        // metodo
        int f = 0;
        try {
            f = Integer.parseInt(c);
        } catch (NumberFormatException err) {
            System.out.println("El valor ingresado es incorrecto");
        }
        
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);*/
       
       // operadores Aritmeticos
       // suna
       double s = 4 + 8.22;
       
       // resta
       double r = 4 - 9.888;
       
       // multiplicacion
       double m = 234 * 321;
       
       // division
       double d = 23 / 98;
       
       // residuo
       double res = 10 % 3;
       
       System.out.println(s);
       System.out.println(r);
       System.out.println(m);
       System.out.println(res);
    }
    
}
